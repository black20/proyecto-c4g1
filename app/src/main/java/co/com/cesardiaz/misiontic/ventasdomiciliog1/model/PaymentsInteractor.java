package co.com.cesardiaz.misiontic.ventasdomiciliog1.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import co.com.cesardiaz.misiontic.ventasdomiciliog1.mvp.PaymentsMVP;

public class PaymentsInteractor implements PaymentsMVP.Model {

    private List<PaymentsMVP.PaymentsInfo> data;

    public PaymentsInteractor() {
        data = Arrays.asList(
                new PaymentsMVP.PaymentsInfo("Cesar Diaz", "Cra 1 # 10 - 54, Pereira, Colombia"),
                new PaymentsMVP.PaymentsInfo("Lizeth Perdomo", "Cra 1 # 10 - 54, Cali, Colombia"),
                new PaymentsMVP.PaymentsInfo("Alejandro Ospina", "Cra 1 # 10 - 54, Bogota, Colombia"),
                new PaymentsMVP.PaymentsInfo("Alexander Latorre", "Cra 1 # 10 - 54"),
                new PaymentsMVP.PaymentsInfo("Ana Perez", "Cra 1 # 10 - 54"),
                new PaymentsMVP.PaymentsInfo("Andres Giraldo", "Cra 1 # 10 - 54"),
                new PaymentsMVP.PaymentsInfo("Edwin Yara", "Cra 1 # 10 - 54"),
                new PaymentsMVP.PaymentsInfo("Francisco Castañeda", "Cra 1 # 10 - 54"),
                new PaymentsMVP.PaymentsInfo("Hugo Dueñas", "Cra 1 # 10 - 54"),
                new PaymentsMVP.PaymentsInfo("Johan Grisales", "Cra 1 # 10 - 54"),
                new PaymentsMVP.PaymentsInfo("Jhon Guevara", "Cra 1 # 10 - 54"),
                new PaymentsMVP.PaymentsInfo("Jose Pacheco", "Cra 1 # 10 - 54"),
                new PaymentsMVP.PaymentsInfo("Juan Suarez", "Cra 1 # 10 - 54"),
                new PaymentsMVP.PaymentsInfo("Nelson Lopez", "Cra 1 # 10 - 54"),
                new PaymentsMVP.PaymentsInfo("Omar Jacobo", "Cra 1 # 10 - 54"),
                new PaymentsMVP.PaymentsInfo("Yeison Camilo", "Cra 1 # 10 - 54"),
                new PaymentsMVP.PaymentsInfo("Yeison Castaño", "Cra 1 # 10 - 54")
        );

    }

    @Override
    public void loadPayments(LoadPaymentsCallback callback) {
        callback.showPaymentsInfo(this.data);
    }
}
