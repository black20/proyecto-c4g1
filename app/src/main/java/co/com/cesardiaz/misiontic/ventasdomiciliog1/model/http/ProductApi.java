package co.com.cesardiaz.misiontic.ventasdomiciliog1.model.http;

import java.util.List;

import co.com.cesardiaz.misiontic.ventasdomiciliog1.model.http.dto.ProductRequest;
import co.com.cesardiaz.misiontic.ventasdomiciliog1.model.http.dto.ProductResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ProductApi {

    @GET("products")
    Call<List<ProductResponse>> getAll();

    @GET("products/{code}")
    Call<ProductResponse> getByCode(@Path("code") String code);

    @POST("products")
    Call<ProductResponse> createProduct(@Body ProductRequest product);
}
