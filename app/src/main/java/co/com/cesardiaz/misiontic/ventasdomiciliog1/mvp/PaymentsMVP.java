package co.com.cesardiaz.misiontic.ventasdomiciliog1.mvp;

import android.app.Activity;
import android.os.Bundle;

import java.util.List;

public interface PaymentsMVP {
    interface Model {
        void loadPayments(LoadPaymentsCallback callback);

        interface LoadPaymentsCallback {
            void showPaymentsInfo(List<PaymentsInfo> paymentsInfo);
        }
    }

    interface Presenter {
        void loadPayments();

        void onNewSaleClick();

        void onItemSelected(PaymentsInfo info);
    }

    interface View {
        Activity getActivity();

        void showProgressBar();

        void hideProgressBar();

        void showNewSaleActivity();

        void showPaymentsInfo(List<PaymentsInfo> paymentsInfo);

        void openLocationActivity(Bundle params);
    }

    class PaymentsInfo {
        private String image;
        private String name;
        private String address;

        public PaymentsInfo(String name, String address) {
            this(null, name, address);
        }

        public PaymentsInfo(String image, String name, String address) {
            this.image = image;
            this.name = name;
            this.address = address;
        }

        public String getImage() {
            return image;
        }

        public String getName() {
            return name;
        }

        public String getAddress() {
            return address;
        }
    }
}
