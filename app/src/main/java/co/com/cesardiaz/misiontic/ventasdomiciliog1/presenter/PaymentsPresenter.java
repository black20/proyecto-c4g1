package co.com.cesardiaz.misiontic.ventasdomiciliog1.presenter;

import android.os.Bundle;

import java.util.List;

import co.com.cesardiaz.misiontic.ventasdomiciliog1.model.PaymentsInteractor;
import co.com.cesardiaz.misiontic.ventasdomiciliog1.mvp.PaymentsMVP;

public class PaymentsPresenter implements PaymentsMVP.Presenter {
    private PaymentsMVP.View view;
    private PaymentsMVP.Model model;

    public PaymentsPresenter(PaymentsMVP.View view) {
        this.view = view;
        this.model = new PaymentsInteractor();
    }

    @Override
    public void loadPayments() {
        view.showProgressBar();
        new Thread(() -> {
            model.loadPayments(new PaymentsMVP.Model.LoadPaymentsCallback() {
                @Override
                public void showPaymentsInfo(List<PaymentsMVP.PaymentsInfo> paymentsInfo) {
                    view.getActivity().runOnUiThread(() -> {
                        view.showPaymentsInfo(paymentsInfo);
                        view.hideProgressBar();
                    });
                }
            });
        }).start();
    }

    @Override
    public void onNewSaleClick() {
        view.showNewSaleActivity();
    }

    @Override
    public void onItemSelected(PaymentsMVP.PaymentsInfo info) {
        Bundle params = new Bundle();
        params.putString("name", info.getName());
        params.putString("address", info.getAddress());

        view.openLocationActivity(params);
    }
}
