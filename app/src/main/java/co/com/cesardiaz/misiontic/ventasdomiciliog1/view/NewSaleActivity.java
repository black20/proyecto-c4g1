package co.com.cesardiaz.misiontic.ventasdomiciliog1.view;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.CancellationToken;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnTokenCanceledListener;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import co.com.cesardiaz.misiontic.ventasdomiciliog1.R;

public class NewSaleActivity extends AppCompatActivity {

    private TextInputLayout tilClient;
    private TextInputEditText etClient;
    private TextInputLayout tilAddress;
    private TextInputEditText etAddress;
    private TextInputLayout tilAmount;
    private TextInputEditText etAmount;
    private TextInputLayout tilNumber;
    private TextInputEditText etnumber;
    private TextInputLayout tilPeriodicity;
    private MaterialAutoCompleteTextView etPeriodicity;
    private TextInputLayout tilPart;
    private TextInputEditText etPart;
    private TextInputLayout tilDate;
    private TextInputEditText etDate;
    private AppCompatButton btSave;

    private Date selectedDate;

    private FusedLocationProviderClient fusedLocationClient;
    private Location location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_sale);

        initUI();
        loadData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestLocation();
    }

    private void initUI() {

        etClient = findViewById(R.id.et_client);

        tilDate = findViewById(R.id.til_date);
        tilDate.setEndIconOnClickListener(v -> onDateClick());

        etDate = findViewById(R.id.et_date);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

    }

    private void requestLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                requestPermissions();
            }
            return;
        }
        fusedLocationClient.getLastLocation()
                .addOnFailureListener(e -> {
                    Log.e("NewSaleActivity", "No pudo obtener la posición", e);
                })
                .addOnSuccessListener(this,
                        location -> {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                // Logic to handle location object
                                this.location = location;
                                Toast.makeText(NewSaleActivity.this,
                                        this.location.getLatitude() + "," + location.getLongitude(),
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(NewSaleActivity.this, "No hay ubicación actualmente",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
    }

    private void loadData() {
        etClient.setText(getIntent().getCharSequenceExtra("Client"));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void requestPermissions() {
        // Obtener permisos para el GPS
        ActivityResultLauncher<String[]> locationPermissionRequest =
                registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(),
                        result -> {
                            Boolean fineLocationGranted = result.getOrDefault(
                                    Manifest.permission.ACCESS_FINE_LOCATION, false);
                            Boolean coarseLocationGranted = result.getOrDefault(
                                    Manifest.permission.ACCESS_COARSE_LOCATION, false);
                            if (fineLocationGranted != null && fineLocationGranted) {
                                // Precise location access granted.
                            } else if (coarseLocationGranted != null && coarseLocationGranted) {
                                // Only approximate location access granted.
                            } else {
                                // No location access granted.
                            }
                        }
                );

        locationPermissionRequest.launch(new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        });

    }


    private void onDateClick() {
        // Seleccion fecha actual
        if (selectedDate == null) {
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            cal.add(Calendar.DAY_OF_MONTH, 1);
            selectedDate = cal.getTime();
        }

        // Restricción de fechas (desde mañana)
        CalendarConstraints constraints = new CalendarConstraints.Builder()
                .setValidator(DateValidatorPointForward.now())
                .build();

        MaterialDatePicker<Long> datePicker = MaterialDatePicker.Builder
                .datePicker()
                .setSelection(selectedDate.getTime())
                .setCalendarConstraints(constraints)
                .setTitleText(R.string.newsale_date)
                .build();
        datePicker.addOnPositiveButtonClickListener(this::onDateSelected);
        datePicker.show(getSupportFragmentManager(), "date");
    }

    private void onDateSelected(Long selection) {
        selectedDate = new Date(selection);

        etDate.setText(SimpleDateFormat.getDateInstance().format(selectedDate));
    }
}