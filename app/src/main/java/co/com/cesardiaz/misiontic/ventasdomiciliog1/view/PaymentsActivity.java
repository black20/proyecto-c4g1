package co.com.cesardiaz.misiontic.ventasdomiciliog1.view;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.progressindicator.LinearProgressIndicator;

import java.util.List;

import co.com.cesardiaz.misiontic.ventasdomiciliog1.R;
import co.com.cesardiaz.misiontic.ventasdomiciliog1.model.repository.FirebaseAuthRepository;
import co.com.cesardiaz.misiontic.ventasdomiciliog1.mvp.PaymentsMVP;
import co.com.cesardiaz.misiontic.ventasdomiciliog1.presenter.PaymentsPresenter;
import co.com.cesardiaz.misiontic.ventasdomiciliog1.view.adapter.PaymentsAdapter;

public class PaymentsActivity extends AppCompatActivity implements PaymentsMVP.View {

    private DrawerLayout drawerLayout;
    private MaterialToolbar appBar;
    private NavigationView navigationDrawer;

    private LinearProgressIndicator piWaiting;
    private RecyclerView rvPayments;
    private FloatingActionButton btnSale;

    private PaymentsMVP.Presenter presenter;
    private PaymentsAdapter paymentsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);

        presenter = new PaymentsPresenter(this);

        initUI();

        presenter.loadPayments();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.app_name)
                .setMessage("Desea cerrar la sesión?")
                .setPositiveButton("Si", (dialog, which) -> {
                    FirebaseAuthRepository.getInstance(PaymentsActivity.this)
                            .logout(new FirebaseAuthRepository.FirebaseAuthCallback() {
                                @Override
                                public void onSuccess() {
                                    Toast.makeText(PaymentsActivity.this,
                                            "Sesión cerrada",
                                            Toast.LENGTH_SHORT).show();
                                    PaymentsActivity.super.onBackPressed();
                                }

                                @Override
                                public void onFailure() {
                                    Toast.makeText(PaymentsActivity.this,
                                            "Error al cerrar la sesión",
                                            Toast.LENGTH_SHORT).show();
                                }
                            });
                })
                .setNegativeButton("No", null);
        builder.create().show();

    }

    private void initUI() {
        drawerLayout = findViewById(R.id.drawer_layout);

        appBar = findViewById(R.id.app_bar);
        appBar.setNavigationOnClickListener(v -> openDrawer());

        navigationDrawer = findViewById(R.id.navigation_drawer);
        navigationDrawer.setNavigationItemSelectedListener(this::navigationitemSelected);

        piWaiting = findViewById(R.id.pi_waiting);

        paymentsAdapter = new PaymentsAdapter();
        paymentsAdapter.setOnItemClickListener(presenter::onItemSelected);

        rvPayments = findViewById(R.id.rv_payments);
        rvPayments.setLayoutManager(new LinearLayoutManager(PaymentsActivity.this));
        rvPayments.setAdapter(paymentsAdapter);

        btnSale = findViewById(R.id.btn_sale);
        btnSale.setOnClickListener(v -> presenter.onNewSaleClick());
    }

    private void openDrawer() {
        drawerLayout.openDrawer(navigationDrawer);
    }

    private boolean navigationitemSelected(MenuItem menuItem) {
        menuItem.setChecked(true);
        Toast.makeText(this, menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        drawerLayout.closeDrawer(navigationDrawer);
        return true;
    }

    @Override
    public Activity getActivity() {
        return PaymentsActivity.this;
    }

    @Override
    public void showProgressBar() {
        piWaiting.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        piWaiting.setVisibility(View.GONE);
    }

    @Override
    public void showNewSaleActivity() {
        Intent intent = new Intent(PaymentsActivity.this, NewSaleActivity.class);
        startActivity(intent);
    }

    @Override
    public void showPaymentsInfo(List<PaymentsMVP.PaymentsInfo> paymentsInfo) {
        paymentsAdapter.setData(paymentsInfo);
    }

    @Override
    public void openLocationActivity(Bundle params) {
        Intent intent = new Intent(PaymentsActivity.this, LocationActivity.class);
        intent.putExtras(params);
        startActivity(intent);
    }
}