package co.com.cesardiaz.misiontic.ventasdomiciliog1.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import co.com.cesardiaz.misiontic.ventasdomiciliog1.R;
import co.com.cesardiaz.misiontic.ventasdomiciliog1.mvp.PaymentsMVP;

public class PaymentsAdapter extends RecyclerView.Adapter<PaymentsAdapter.ViewHolder> {
    private List<PaymentsMVP.PaymentsInfo> data;
    private OnItemClickListener onItemClickListener;

    public PaymentsAdapter() {
        this.data = new ArrayList<>();
    }

    public void setData(List<PaymentsMVP.PaymentsInfo> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_payments, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PaymentsMVP.PaymentsInfo item = data.get(position);

        // Asignar click listener a elemento
        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener(v -> onItemClickListener.onItemClick(item));
        }

        //TODO holder.getIvClient().setImageIcon();
        holder.getTvClient().setText(item.getName());
        holder.getTvAddress().setText(item.getAddress());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivClient;
        private TextView tvClient;
        private TextView tvAddress;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivClient = itemView.findViewById(R.id.iv_client);
            tvClient = itemView.findViewById(R.id.tv_client);
            tvAddress = itemView.findViewById(R.id.tv_address);
        }

        public ImageView getIvClient() {
            return ivClient;
        }

        public TextView getTvClient() {
            return tvClient;
        }

        public TextView getTvAddress() {
            return tvAddress;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(PaymentsMVP.PaymentsInfo info);
    }
}
